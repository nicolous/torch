/**
 *
 */
package com.brainstorm.torch.ui.api;

/**
 * Camera controller.
 * @author Nico
 */
public interface TORCH_CameraController
{
    /**
     * Determine if the camera supports the torch mode.
     * @return <code>true</code> if it's supported, <code>false</code> otherwise.
     */
    boolean doesSupportTorch();

    /**
     * Turn on/off the torch.
     * @param on <code>true</code> to turn it on, <code>false</code> to turn it off.
     * @throws Exception Any problem when interacting with the camera.
     */
    void setTorch(boolean on) throws Exception;

}
