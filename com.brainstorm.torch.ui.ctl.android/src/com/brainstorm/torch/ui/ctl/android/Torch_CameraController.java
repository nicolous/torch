/**
 *
 */
package com.brainstorm.torch.ui.ctl.android;

import java.util.List;

import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.util.Log;

import com.brainstorm.torch.ui.api.TORCH_CameraController;

// Supposed not to work with Samsung devices. Possible workaround:
// http://stackoverflow.com/questions/3878294/camera-parameters-flash-mode-torch-replacement-for-android-2-1.

/**
 * Camera controller. Not thread safe.
 * @author Nico
 */
public class Torch_CameraController implements TORCH_CameraController
{
    /** Log tag. */
    private static final String TAG = Torch_CameraController.class.getSimpleName();

    /** Unique instance of this class. */
    private static Torch_CameraController _instance;

    /** Android camera. */
    private Camera _camera;

    /**
     * Constructor.
     * @throws RuntimeException No camera found.
     */
    private Torch_CameraController() throws Exception
    {
        testCamera();
    }

    /**
     * Get an instance of the controller.
     * @return Singleton.
     * @throws Exception Any error when interacting with the camera.
     */
    public static Torch_CameraController getInstance() throws Exception
    {
        if (_instance == null)
            _instance = new Torch_CameraController();
        return _instance;
    }

    /**
     * @{inheritDoc
     */
    @Override
    public boolean doesSupportTorch()
    {
        boolean supports = false;

        final boolean acquireReleaseCamera = _camera == null;
        if (acquireReleaseCamera)
            acquireCamera();

        if (_camera == null)
            Log.d(TAG, "Camera is null.");
        else
        {
            final Parameters parameters = _camera.getParameters();
            final List<String> flashModes = parameters.getSupportedFlashModes();
            if (flashModes != null)
            {
                supports = flashModes.contains(Parameters.FLASH_MODE_TORCH);
            }

            if (acquireReleaseCamera)
                releaseCamera();
        }

        return supports;
    }

    /**
     * @{inheritDoc
     */
    @Override
    public void setTorch(final boolean torchOn) throws Exception
    {
        // Acquire/Release the camera at each state change because some devices seem to need it.
        // (Nexus 5, some Samsung)

        if (torchOn)
        {
            acquireCamera();

            _camera.startPreview();

            final Parameters parameters = _camera.getParameters();
            parameters.setFlashMode(Parameters.FLASH_MODE_TORCH);
            _camera.setParameters(parameters);

            _camera.setPreviewTexture(new SurfaceTexture(0)); // as required by startPreview
        }
        else
        {
            final Parameters parameters = _camera.getParameters();

            _camera.stopPreview();
            parameters.setFlashMode(Parameters.FLASH_MODE_OFF);
            _camera.setParameters(parameters);

            releaseCamera();
        }
    }

    /**
     * Try to acquire/Release the camera.
     * @throws RuntimeException Any error when interacting with the camera.
     */
    private void testCamera()
    {
        acquireCamera();
        releaseCamera();
    }

    /**
     * Acquire the camera.
     * @throws RuntimeException Any error when interacting with the camera.
     */
    private void acquireCamera()
    {
        if (_camera == null)
        {
            _camera = Camera.open();
        }
    }

    /**
     * Release the camera.
     * @throws RuntimeException Any error when interacting with the camera.
     */
    private void releaseCamera()
    {
        if (_camera != null)
        {
            _camera.release();
            _camera = null;
        }
    }
}
