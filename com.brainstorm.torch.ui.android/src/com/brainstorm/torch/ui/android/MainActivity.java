package com.brainstorm.torch.ui.android;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ToggleButton;

import com.brainstorm.torch.ui.api.TORCH_CameraController;
import com.brainstorm.torch.ui.ctl.android.Torch_CameraController;

/**
 * Torch activity. <br>
 * The activity handles configuration and screen changes.
 * @author Nico
 */
public class MainActivity extends Activity
{
    /** Log tag. */
    private static final String TAG = MainActivity.class.getSimpleName();

    /** Camera controller instance. */
    private TORCH_CameraController _camera;

    /** Toggle torch button . */
    private ToggleButton _toggleFlashButton;

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onCreate(final Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        _toggleFlashButton = (ToggleButton) findViewById(R.id.toggleFlashButton);

        try
        {
            _camera = Torch_CameraController.getInstance();
        }
        catch (final Exception e)
        {
            reportException(e);
        }

        if (_camera != null)
        {
            if (_camera.doesSupportTorch())
            {
                initView();

                // By default, the toggle button is disabled.
                _toggleFlashButton.setEnabled(true);
            }
            else
                reportInfo(getText(R.string.torch_not_supported));
        }
    }

    /**
     * @{inheritDoc
     */
    @Override
    protected void onPause()
    {
        super.onPause();

        if (_toggleFlashButton.isChecked())
            _toggleFlashButton.setChecked(false);
    }

    /**
     * @{inheritDoc
     */
    @Override
    protected void onResume()
    {
        super.onResume();

        // Toggle the torch at start
        _toggleFlashButton.setChecked(true);
    }

    /**
     * @{inheritDoc
     */
    @Override
    public boolean onCreateOptionsMenu(final Menu menu)
    {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    /**
     * @{inheritDoc
     */
    @Override
    public boolean onOptionsItemSelected(final MenuItem item)
    {
        final boolean consumed;
        final int id = item.getItemId();
        if (id == R.id.action_about)
        {
            final AlertDialog.Builder alertBuilder = new AlertDialog.Builder(MainActivity.this);
            final AlertDialog alert = alertBuilder.create();
            alert.setView(getLayoutInflater().inflate(R.layout.about, null));
            alert.setButton(DialogInterface.BUTTON_POSITIVE, getText(android.R.string.ok),
                    (DialogInterface.OnClickListener) null);
            alert.show();
            consumed = true;
        }
        else
            consumed = super.onOptionsItemSelected(item);
        return consumed;
    }

    /**
     * Report an exception to the user, and log it. Takes care to run UI things on the UI thread.
     * @param throwable Exception to report.
     */
    public void reportException(final Throwable throwable)
    {
        Log.e(TAG, throwable.getMessage(), throwable);

        runOnUiThread(new Runnable()
        {
            @Override
            public void run()
            {
                final AlertDialog.Builder alertBuilder = new AlertDialog.Builder(MainActivity.this);
                final AlertDialog alert = alertBuilder.create();
                alert.setMessage(throwable.getLocalizedMessage());
                alert.setButton(DialogInterface.BUTTON_POSITIVE, getText(android.R.string.ok),
                        (DialogInterface.OnClickListener) null);
                alert.show();
            }
        });
    }

    /**
     * Report an information to the user, and log it. Takes care to run UI things on the UI thread.
     * @param info Information to report.
     */
    public void reportInfo(final CharSequence info)
    {
        Log.i(TAG, info.toString());

        runOnUiThread(new Runnable()
        {
            @Override
            public void run()
            {
                final AlertDialog.Builder alertBuilder = new AlertDialog.Builder(MainActivity.this);
                final AlertDialog alert = alertBuilder.create();
                alert.setMessage(info);
                alert.setButton(DialogInterface.BUTTON_POSITIVE, getText(android.R.string.ok),
                        (DialogInterface.OnClickListener) null);
                alert.show();
            }
        });
    }

    /**
     * Initialize the view and its callbacks.
     */
    private void initView()
    {
        _toggleFlashButton.setOnCheckedChangeListener(new OnCheckedChangeListener()
        {
            /**
             * @{inheritDoc
             */
            @Override
            public void onCheckedChanged(final CompoundButton buttonView, final boolean isChecked)
            {
                toggleTorch(isChecked);
            }
        });

        // Don't fade out the screen
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    /**
     * Toggle the torch.
     * @param toggleOn <code>true</code> to turn it on, <code>false</code> to turn it off.
     */
    private void toggleTorch(final boolean toggleOn)
    {
        // Turn the torch on/off takes time, so run it with an async task
        new AsyncTask<Void, Void, Void>()
        {
            /**
             * @{inheritDoc
             */
            @Override
            protected Void doInBackground(final Void... params)
            {
                try
                {
                    _camera.setTorch(toggleOn);
                }
                catch (final Exception e)
                {
                    reportException(e);
                }
                return null;
            }
        }.execute();
    }
}
